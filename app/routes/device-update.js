'use strict';
const net = require('net');
const PORT = 2223;
const ADDRESS = '0.0.0.0';
const { DeviceParam, DeviceParamHistory } = require('../models');
const { checkRulses } = require('../lib');
const uuid = require('uuid-pure').newId;
const _ = require('underscore');
const express = require('express');
const request = require('request-promise-native');
const app = express();
const morgan = require('morgan');
const geoip = require('geoip-lite');
//const TestKey = require('./redis-ttl')
const clients = [];
const deviceIpList = new Map();
const redis = require('redis');
const CONF = { db: 0 };
const pub = redis.createClient(CONF);
pub.send_command('config', ['set', 'notify-keyspace-events', 'Ex'], SubscribeExpired);

function SubscribeExpired(e, r) {
    const sub = redis.createClient(CONF);
    const expired_subKey = '__keyevent@' + CONF.db + '__:expired';
    console.log(expired_subKey)
    sub.subscribe(expired_subKey, function() {
        sub.on('message', async(chan, msg) => {
            try {
                //const result = await DeviceParam.findOneAndUpdate({ socketIp: msg }, { isOnline: false }, { new: true }).exec();
                //deviceIpList.delete(result.mac);
            } catch (error) {
                console.log(error)
            }
        })
    })
};

//function TestKey(key, value, ex) {
//  console.log(key, value, ex, 'sub')
// pub.set(key, value)
// pub.expire(key, ex)
//};



let server = net.createServer(onClientConnected);
server.listen(PORT, ADDRESS);

//function onClientConnected(socket) {
//    console.log(`New client: ${socket.remoteAddress}:${socket.remotePort}`);
//    socket.destroy();
//}

console.log(`Server started at: ${ADDRESS}:${PORT}`);


async function onClientConnected(socket) {
   try{
    socket.name = `${socket.remoteAddress}:${socket.remotePort}`;
    clients.push(socket, 'device data');
//    TestKey(socket.name, socket.name, 60)
    socket.on('data', async (data) => {
        var m = data.toString().replace(/[\n\r]*$/, '');
//        console.log(m);
        var n = m.slice(0, 3);
        var date = new Date();
        var timestamp = date.getTime();
        var newTimeStampInSec = parseInt(timestamp / 1000)

        if (m === '__heartbeat__' || n === 'SUB') {
            if (m === '__heartbeat__') {
                return socket.write("__heartbeat__")
            }
            let mac = m.slice(4, 16);
	    if(mac === 'F0FE6B765FC4' || mac === 'F0FE6B76643A' || mac === 'F0FE6BB6FD5A'){console.log(m, 'first')}
            let a = [{ 'date': new Date(), 'from': 'tcpServer75' }]

            await DeviceParam.findOneAndUpdate({ mac }, {updatedFrom: a, isOnline: true }, { new: true });
            var obj = { 'timestamp': newTimeStampInSec, 'mac': mac }
            await request.post({ url: 'http://3.105.183.233:3000/SMT-400/timeStamp', json: true, body: obj });

            deviceIpList.set(mac, socket);
            return socket.write(`{"result":"ok"}`);
        }
        if (m.length >= 20) {
            let mac = m.slice(8, 20);
	    if(mac === 'F0FE6B765FC4' || mac === 'F0FE6B76643A' || mac === 'F0FE6BB6FD5A'){console.log(m, 'second')}
            let a = [{ 'date': new Date(), 'from': 'tcpServer87' }]

            await DeviceParam.findOneAndUpdate({ mac }, {updatedFrom: a, isOnline: true }, { new: true });
            var obj = { 'timestamp': newTimeStampInSec, 'mac': mac }
            await request.post({ url: 'http://3.105.183.233:3000/SMT-400/timeStamp', json: true, body: obj });
        }
        var l = m;
        if (m) {
            socket.data = { msg: m };
            insertData(socket);
        } else {
            console.log(`${socket.name} : ${l} "check the data"`);
            broadcast(socket.name + "> " + l + " check the data", socket);
            socket.write(`${l}. check the data!\n`);
        }

    });

    socket.on('end', async () => {
        console.log('server disconnected', socket.name)
	var obj = {'socketName': socket.name}
        //await request.post({ url: 'http://3.105.183.233:3000/SMT-400/deviceStatus', json: true, body: obj });
        clients.splice(clients.indexOf(socket), 1);
        //clients.splice(clients.indexOf(socket), 1);
        // DeviceParam.findOneAndUpdate({ socketIp: socket.name }, { isOnline: false }, { new: true}).exec()
        //  .then(data => {
        //   deviceIpList.delete(data.mac, socket);
        // broadcast(socket.name + "> " + l + " check the data", socket);
        // })
        // .catch(error => {
        //   console.log(error)
        //})
    });

//    socket.setTimeout(6000);
  //  socket.on('timeout', () => { console.log('socket timeout'); socket.end(); });
    socket.on('error', function (err) {
        console.log(err.stack)
	console.log('error')
    });

    socket.on('uncaughtException', function (err) {
        console.log(err.stack)
        console.log('uncaught error')
    });
  } catch (err) {
	console.log(err)
  }
}


/**************/

async function insertData(socket) {
    const controlParameters = [
        'equip_mode',
        'fan_speed',
        'temp_cool',
        'temp_heat',
        'occupied',
        'relay_status',
        'wifi_lost'
    ];



    const getData = socket.data.msg || {};
    try {
	var date = new Date();
        var timestamp = date.getTime();
        var newTimeStampInSec = parseInt(timestamp / 1000)

        const ObjData = JSON.parse(getData);
        var { mac, MsgID, cmd } = ObjData;
//	if(mac === 'F0FE6BB6FEA4'){console.log('insert data', mac)}
        if (mac.length !== 12) {
            socket.write(`'MacId Must Be 12 Characters'`);
            return;
        }
        deviceIpList.set(mac, socket);
        let a = [{ 'date': new Date(), 'from': 'tcpServerInsert' }]

        await DeviceParam.findOneAndUpdate({ mac }, {updatedFrom: a, isOnline: true }, { new: true });
        let getLocation = socket.name.split(':')
        var ip = getLocation[0];
        var geo = geoip.lookup(ip) || {};
        Object.assign(ObjData, { socketIp: socket.name, ipDetails: geo, isOnline: true });
        // New device Add

        const findValue = await DeviceParam.findOne({ mac }).exec()
            //	console.log(!findValue, 'thermostat')
        if (!findValue) {
            //var date = new Date();
            //var timestamp = date.getTime();
            //var newTimeStampInSec = parseInt(timestamp / 1000)
            Object.assign(ObjData, { 'timeStamp': newTimeStampInSec })

            const deviceParam = new DeviceParam(ObjData);
            await deviceParam.save();
            const deviceParamHistory = new DeviceParamHistory(ObjData);
            await deviceParamHistory.save();
            socket.write(`{"result":"ok"}`);
            return;
        }



        // Every 20Sec Insert values In Collection

        const checkControlValue = controlParameters.every(value => ObjData[value] === findValue[value]);
        const { device_name, site_name, aux_relay } = findValue;
        Object.assign(ObjData, { device_name, site_name, aux_relay });

        if (checkControlValue) {
            Object.assign(ObjData, { MsgID: "" });

	    //var date = new Date();
            //var timestamp = date.getTime();
            //var newTimeStampInSec = parseInt(timestamp / 1000)
//            let a = [{ 'date': new Date(), 'from': 'tcpServerInsert206' }]

            Object.assign(ObjData, {updatedFrom: a, 'timeStamp': newTimeStampInSec })

            var result = await DeviceParam.findOneAndUpdate({ mac }, ObjData);
            const deviceParamHistory = new DeviceParamHistory(ObjData);
            await deviceParamHistory.save();

            //var dt = new Date();
            //const result1 = await DeviceParamHistory.find({ mac: result.mac });
            //result1.forEach(async(ele) => {
            //    var recDate = ele.createdAt;
            //    const diffTime = Math.abs(dt - recDate);
            //    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
            //    if (diffDays > 7) {
            //        await DeviceParamHistory.findOneAndRemove({ _id: ele._id })
            //    }
            //})
            return socket.write(`{"result":"ok"}`);
        }

//	if(mac === 'F0FE6BB6FEA4'){console.log("line 2nd console blob", mac)}
        if (ObjData.MsgID !== undefined || cmd === 'time') {
            socket.write(`{"result":"ok"}`);
            if (findValue.MsgID !== "") {
                Object.assign(ObjData, { MsgID: "" });

		//var date = new Date();
                //var timestamp = date.getTime();
                //var newTimeStampInSec = parseInt(timestamp / 1000)
//                let a = [{ 'date': new Date(), 'from': 'tcpServerInsert237' }]

                Object.assign(ObjData, {updatedFrom: a, 'timeStamp': newTimeStampInSec })

                var result = await DeviceParam.findOneAndUpdate({ mac, MsgID: findValue.MsgID }, ObjData, { new: true });
                const deviceParamHistory1 = new DeviceParamHistory(ObjData);
                await deviceParamHistory1.save();
                socket.write(`{"result":"ok"}`);
                await request.post({ url: 'http://3.105.183.233:3000/api/device-tcp', json: true, body: result });

          //      var dt = new Date();
          //      const result1 = await DeviceParamHistory.find({ mac: result.mac });
          //      result1.forEach(async(ele) => {
          //          var recDate = ele.createdAt;
          //          const diffTime = Math.abs(dt - recDate);
          //          const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
          //          if (diffDays > 7) {
          //              await DeviceParamHistory.findOneAndRemove({ _id: ele._id })
          //          }
          //      })
                return;
            }

            return socket.write(`{"result":"ok"}`);

        }
        //console.log('hello lets check if im working')

        // Change Value Update Here

        var MsgID = uuid(15);
        Object.assign(ObjData, { MsgID });

	//var date = new Date();
        //var timestamp = date.getTime();
        //var newTimeStampInSec = parseInt(timestamp / 1000)
//        let a = [{ 'date': new Date(), 'from': 'tcpServerInsert272' }]

        Object.assign(ObjData, {updatedFrom: a, 'timeStamp': newTimeStampInSec })

        var result = await DeviceParam.findOneAndUpdate({ mac }, ObjData, { new: true });
        const deviceParamHistory1 = new DeviceParamHistory(ObjData);
        await deviceParamHistory1.save();
//	if(mac === 'F0FE6BB6FEA4'){console.log("2nd server...... haha", mac)}
        if (!result) {
            socket.write(`{message: "Not Connected to the database"}`);
            return;
        }
//	if(mac === 'F0FE6BB6FEA4'){console.log("3rd console bla......", mac)}
        const newArray = [
            ['MsgID', MsgID]
        ];
//	if(mac === 'F0FE6BB6FEA4'){console.log("line -1 before posting to aws server......", mac)}
        controlParameters.forEach((data) => {
            if (!(findValue[data] == result[data])) {
                return newArray.push([data, result[data]])
            }
        });
        const finalResult = JSON.stringify(_.object(newArray))
//	if(mac === 'F0FE6BB6FEA4'){console.log("line 0 before posting to aws server......", mac)}
        socket.write(finalResult);
        Object.assign(finalResult);
        checkRulses(finalResult, mac)

//	if(mac === 'F0FE6BB6FEA4'){console.log("line before posting to aws server......", mac)}
            //	await request.post({ url: 'http://3.105.183.233:3000/api/device-tcp', json: true, body: result });
        await request.put({ url: 'http://3.105.183.233:3000/api/schedule/tcp', json: true, body: result }); // to update scheduleRunning to false if any schedule is running for the same Mac id
	// if(mac === 'F0FE6BB6FEA4'){console.log(resultsCH)}
        return;
    } catch (error) {
        socket.write("");
    }
}


function broadcast(message, sender) {
    clients.forEach(function(client) {
        if (client === sender) return;
        client.write(message);
    });
    process.stdout.write(message)
}



function SubscribeExpired(e, r) {
    const sub = redis.createClient(CONF);
    const expired_subKey = '__keyevent@' + CONF.db + '__:expired';
    sub.subscribe(expired_subKey, function() {
        sub.on('message', async(chan, msg) => {

            try {
                //const result = await DeviceParam.findOneAndUpdate({ socketIp: msg }, { isOnline: false }, { new: true }).exec();
                const result = await DeviceParam.findOne({ socketIp: msg }, { new: true }).exec();
                if (!result || !result.mac) {
                    return;
                }
                deviceIpList.delete(result.mac);
            } catch (error) {
                console.log(error)
            }
        })
    })
};

function TestKey(key, value, ex) {
    console.log(key, value, ex, 'data')
    pub.set(key, value)
    pub.expire(key, ex)
};


module.exports = app => {
    app.post('/', async(req, res) => {
        try {
	   // if(req.body.mac === 'F0FE6B765FB4' || req.body.mac === 'F0FE6BB6FEA4'){
	    //  console.log('function triggered', req.body)
	   // }
            const { mac } = req.body;
            const client = deviceIpList.get(mac);
            if (client === undefined) {
                res.send('mac id not matched')
                return;
            }else{
            	const resData = JSON.stringify(req.body.data)
		try {
            		client.write(`${resData}\n`);
            		res.send('ok');
			return;
		} catch (err){
			console.log(err)
		}
	    }
        } catch (error) {
            console.log(error)
        }
    })
}
