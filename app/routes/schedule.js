'use strict';
const express = require('express');
const router = express.Router();
const { Schedule, UserProfiles, DeviceParam } = require('../models');
const { LocalProfile } = UserProfiles;
const uuid = require('uuid-pure').newId;
const _ = require('underscore');

// mob or web or device Insert Value

router.post('/', async(req, res, next) => {
    const { mac, userId } = req.body;
    console.log(mac, userId)
    try {
        const result = await LocalProfile.findOne({ userId, mac });
        if (!result) {
            const error = new Error('Given User Not Exist');
            error.status = 400;
            next(error);
        }
        const schedule = new Schedule(req.body);
        const setScheduleValue = await schedule.save();
        res.json(setScheduleValue);
    } catch (error) {
        next(error);
    }
});

// mob or web or device get Value



module.exports = router;
