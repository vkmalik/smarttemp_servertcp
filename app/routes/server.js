'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = '8000';


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

require('./device-update')(app);




app.listen(port, () => {
    console.log(`App listen Port @ ${port}`)
})
