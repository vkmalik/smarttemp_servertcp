'use strict';
const net = require('net');
const PORT = 2223;
const ADDRESS = '0.0.0.0';
const { DeviceParam, DeviceParamHistory } = require('../models');
const { checkRulses } = require('../lib');
const uuid = require('uuid-pure').newId;
const _ = require('underscore');
const express = require('express');
const request = require('request-promise-native');
const app = express();
const geoip = require('geoip-lite');
const morgan = require('morgan');
//const TestKey = require('./redis-ttl')
const clients = [];
const deviceIpList = new Map();
const redis = require('redis');
const CONF = { db: 0 };
const pub = redis.createClient(CONF);
  pub.send_command('config', ['set', 'notify-keyspace-events', 'Ex'], SubscribeExpired);

//function SubscribeExpired(e, r) {
  //  const sub = redis.createClient(CONF);
   // const expired_subKey = '__keyevent@' + CONF.db + '__:expired';
   // console.log(expired_subKey)
   // sub.subscribe(expired_subKey, function () {
     //   sub.on('message', async (chan, msg) => {
       //     try {
                //const result = await DeviceParam.findOneAndUpdate({ socketIp: msg }, { isOnline: false }, { new: true }).exec();
                //deviceIpList.delete(result.mac);
         //   } catch (error) {
           //     console.log(error)
           // }
       // })
   // })
//};

//function TestKey(key, value, ex) {
//  console.log(key, value, ex, 'sub')
// pub.set(key, value)
// pub.expire(key, ex)
//};



let server = net.createServer(onClientConnected);
server.listen(PORT, ADDRESS);

//function onClientConnected(socket) {
  //  console.log(`New client: ${socket.remoteAddress}:${socket.remotePort}`);
   // socket.destroy();
//}

console.log(`Server started at: ${ADDRESS}:${PORT}`);


function onClientConnected(socket) {
    console.log("device request found")
console.log('device running');
    socket.name = `${socket.remoteAddress}:${socket.remotePort}`;
    clients.push(socket, 'device data');
    console.log(socket.name)
    TestKey(socket.name, socket.name, 60)
    socket.on('data', (data) => {
        var m = data.toString().replace(/[\n\r]*$/, '');
        var n = m.slice(0, 3);
        if (m === '__heartbeat__' || n === 'SUB') {
 console.log(m, 'online1');

            if (m === '__heartbeat__') {
		 console.log(m, 'online2');

                return socket.write("__heartbeat__")
            }
 else
        { console.log('offlineeee1');
}

            let mac = m.slice(4, 16);
            DeviceParam.findOneAndUpdate({ mac }, { isOnline: true }, { new: true });
            deviceIpList.set(mac, socket);
            return socket.write(`{"result":"ok"}`);
        }
	else
	{ console.log('offlineeee2');
}
        var l = m;
        if (m) {
            socket.data = { msg: m };
            insertData(socket);
//console.log('insert');
        } else {
//console.log('no connection');
            console.log(`${socket.name} : ${l} "check the data"`);
            broadcast(socket.name + "> " + l + " check the data", socket);
            socket.write(`${l}. check the data!\n`);
        }

    });

    socket.on('end', () => {
        clients.splice(clients.indexOf(socket), 1);
//console.log('end')
        //clients.splice(clients.indexOf(socket), 1);
        // DeviceParam.findOneAndUpdate({ socketIp: socket.name }, { isOnline: false }, { new: true}).exec()
        //  .then(data => {
        //   deviceIpList.delete(data.mac, socket);
        // broadcast(socket.name + "> " + l + " check the data", socket);
        // })
        // .catch(error => {
        //   console.log(error)
        //})
    });

}


/**************/

async function insertData(socket) {
    const controlParameters = [
        'equip_mode',
        'fan_speed',
        'temp_cool',
        'temp_heat',
        'occupied',
        'relay_status',
        'wifi_lost'
    ];



    const getData = socket.data.msg || {};
    try {
        const ObjData = JSON.parse(getData);
        var { mac, MsgID, cmd } = ObjData;
        if (mac.length !== 12) {
            socket.write(`'MacId Must Be 12 Characters'`);
            return;
        }
        //console.log(mac)
        deviceIpList.set(mac, socket);
        await DeviceParam.findOneAndUpdate({ mac }, { isOnline: true }, { new: true });
        let getLocation = socket.name.split(':')
        var ip = getLocation[0];
        var geo = geoip.lookup(ip) || {};
        Object.assign(ObjData, { socketIp: socket.name, ipDetails: geo, isOnline: true });
        // New device Add

        const findValue = await DeviceParam.findOne({ mac }).exec()
        if (!findValue) {
            const deviceParam = new DeviceParam(ObjData);
            await deviceParam.save();
            const deviceParamHistory = new DeviceParamHistory(ObjData);
            await deviceParamHistory.save();
            socket.write(`{"result":"ok"}`);
            return;
        }



        // Every 20Sec Insert values In Collection

        const checkControlValue = controlParameters.every(value => ObjData[value] === findValue[value]);
        const { device_name, site_name, aux_relay } = findValue;
        Object.assign(ObjData, { device_name, site_name, aux_relay });

        if (checkControlValue) {
            Object.assign(ObjData, { MsgID: "" });
            var result = await DeviceParam.findOneAndUpdate({ mac }, ObjData);
            const deviceParamHistory = new DeviceParamHistory(ObjData);
            await deviceParamHistory.save();
            return socket.write(`{"result":"ok"}`);
        }


        if (ObjData.MsgID !== undefined || cmd === 'time') {
            socket.write(`{"result":"ok"}`);
            if (findValue.MsgID !== "") {
                Object.assign(ObjData, { MsgID: "" });
                var result = await DeviceParam.findOneAndUpdate({ mac, MsgID: findValue.MsgID }, ObjData, { new: true });
                const deviceParamHistory1 = new DeviceParamHistory(ObjData);
                await deviceParamHistory1.save();
                socket.write(`{"result":"ok"}`);
                await request.post({ url: 'http://13.238.117.231:3000/api/device-tcp', json: true, body: result });
                return;
            }

            return socket.write(`{"result":"ok"}`);

        }


        // Change Value Update Here

        var MsgID = uuid(15);
        Object.assign(ObjData, { MsgID });
        var result = await DeviceParam.findOneAndUpdate({ mac }, ObjData, { new: true });
        const deviceParamHistory1 = new DeviceParamHistory(ObjData);
        await deviceParamHistory1.save();
        if (!result) {
            socket.write(`{message: "Not Connected to the database"}`);
            return;
        }
        const newArray = [
            ['MsgID', MsgID]
        ];
        controlParameters.forEach((data) => {
            if (!(findValue[data] == result[data])) {
                return newArray.push([data, result[data]])
            }
        });
        const finalResult = JSON.stringify(_.object(newArray))
        socket.write(finalResult);
        Object.assign(finalResult);
        checkRulses(finalResult, mac)
        // await request.post({ url: 'http://13.238.117.231:3000/api/device-tcp', json: true, body: result });
	 await request.put({ url: 'http://13.238.117.231:3000/api/schedule/tcp', json: true, body: result }); // to update scheduleRunning to false if any schedule is running for the same Mac id
        return;
    } catch (error) {
        socket.write("");
    }
}


function broadcast(message, sender) {
    clients.forEach(function (client) {
        if (client === sender) return;
        client.write(message);
    });
    process.stdout.write(message)
}



function SubscribeExpired(e, r) {
    const sub = redis.createClient(CONF);
    const expired_subKey = '__keyevent@' + CONF.db + '__:expired';
    sub.subscribe(expired_subKey, function () {
        sub.on('message', async (chan, msg) => {

            try {
                const result = await DeviceParam.findOneAndUpdate({ socketIp: msg }, { isOnline: false }, { new: true }).exec();
                if (!result || !result.mac) {
                    return;
                }
                deviceIpList.delete(result.mac);
console.log('off line', msg);
		await request.post({ url: 'http://13.238.58.241:8000', json: true, body: result });
            } catch (error) {
                console.log(error)
            }
        })
    })
};

function TestKey(key, value, ex) {
    console.log(key, value, ex, 'data')
    pub.set(key, value)
    pub.expire(key, ex)
};


module.exports = app => {
    app.post('/', async (req, res) => {
        try {
            const { mac } = req.body;
            const client = deviceIpList.get(mac);
            if (client === undefined) {
                res.send('mac id not matched')
                return;
            }
            const resData = JSON.stringify(req.body.data)
            client.write(`${resData}\n`);
            res.send('ok');
        }
        catch (error) {
            console.log(error)
        }
    })
}
