'use strict';
const redis = require('redis');
const { DeviceParam } = require('../models');
const CONF = { db: 0 };
const pub = redis.createClient(CONF);
pub.send_command('config', ['set', 'notify-keyspace-events', 'Ex'], SubscribeExpired);

function SubscribeExpired(e, r) {
    const sub = redis.createClient(CONF);
    const expired_subKey = '__keyevent@' + CONF.db + '__:expired';
    sub.subscribe(expired_subKey, function() {
        sub.on('message', async (chan, msg) => {

            try {
                const result = await DeviceParam.findOneAndUpdate({ socketIp: msg }, { isOnline: false }, { new: true }).exec();
                // deviceIpList.delete(result.mac);
            } catch (error) {
                console.log(error)
            }
        })
    })
};

function TestKey(key, value, ex) {
	console.log(key, value, ex, 'data')
    pub.set(key, value)
    pub.expire(key, ex)
};


module.exports = TestKey;
