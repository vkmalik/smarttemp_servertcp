'use strict';

const deviceRouter = require('./device-update');
const scheduleRouter = require('./schedule');


module.exports = app => {
    app.use('/SMT-400', deviceRouter);
    app.use('/api/schedule', scheduleRouter);

    app.use(errorHandlingMiddleware);
};
