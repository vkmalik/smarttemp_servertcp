'use strict';
const {  DeviceRules } = require('../models');
const { sendContact } = require('../helpers/mail')
async function checkRulses(deviceData, mac){
    const getValues = JSON.parse(deviceData);
    const getKeys = Object.keys(getValues)
    const checkValue = getKeys[1] === 'temp_cool' ? 'Cool set Temperature' : 'Heat set Temperature';
    const getRulesResult = await DeviceRules.find({ mac, ifData: checkValue });
  if(getRulesResult.length === 0){
      return
  }
  getRulesResult.forEach( async (ele) => {
    if(ele.isData === 'Greater' && getValues[getKeys[1]] > ele.valueData){
      Object.assign(ele, { currentValue: getValues[getKeys[1]]});
        sendContact.send(await ele);
      return;
    }
    if(ele.isData === 'Less' && getValues[getKeys[1]] < ele.valueData){
      Object.assign(ele, { currentValue: getValues[getKeys[1]]});
        sendContact.send(await ele);
      return;
    }
    if(ele.isData === 'Equal Too' && getValues[getKeys[1]] === ele.valueData){
      Object.assign(ele, { currentValue: getValues[getKeys[1]]});
        sendContact.send(await ele);
        return;  
    }
  })
}

module.exports = checkRulses;
