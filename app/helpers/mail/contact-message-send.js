'use strict';
const sendMail = require('../../lib/send-mail');

// const defultEmail = 'admin@smarttem.com.au';

async function send(data) {
    const { email, ruleName, device_name, ifData, isData, valueData, currentValue, messageOfEmail, createdAt, Created } = data || {};
    const html1 = `<p>Dear ${ Created },</p><p> ${ messageOfEmail } a value of ${ valueData } on ${ createdAt }. Please take necessary action. </p>
    <p>Regards,</p>
    <p>SmartTemp AUS P/L</p>
    <p>www.smarttemp.com.au</p>
    <p>support@smarttemp.com.au</p>`
    console.log(email, html1)
    sendMail(email, {
        html: html1,
        subject: `SMT-400 - ${ device_name } - Threshold reached for rule ${ruleName}`
    });
    return;
}


module.exports = {
    send,
};
