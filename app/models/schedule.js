'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = { timestamps: true };


const requiredFieldMessage = (filed) => {
    let message = `${filed} Should Not Be Empty`;
    return [true, message]
 }


const ScheduleSchema = new Schema({
    mac: {type: String, required: requiredFieldMessage('Mac')},   
    equip_mode: {type: String, default: ''},
    fan_speed: {type: String, default: ''},
    temp_cool: {type: String, default: ''},
    temp_heat: {type: String, default: ''},
    occupied: {type: String, default:''},
    relay_status: {type: String, default: ''},
    wifi_lost: {type: String, default:''},
    scheduled: { type: String, default: ''},
    }, options);






module.exports = mongoose.model('tbl_smt400_schedules', ScheduleSchema);
