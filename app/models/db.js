'use strict';
const mongoose = require('mongoose');
const config = require('config');
const dbURL = 'mongodb://user:user123@172.31.5.32:27017/db_smarttemp'


async function connectToDB() {
    try {
        await mongoose.connect(dbURL, { useNewUrlParser: true });
        console.log('Successfully Connected To DataBase');
    } catch (error) {
        console.error(error, 'Database Connection Failed');
        process.exit(1);
    }

}

connectToDB();




const db = mongoose.connection;
// db.on('error', console.error('connection error while connecting to DB'));
// db.once('open', function() {
// 	console.log('Succefully Connected To DB');
// });


module.exports = mongoose;
