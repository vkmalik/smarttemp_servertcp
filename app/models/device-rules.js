'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = { timestamps: true, versionKey: false };
const uuid = require('uuid');


const requiredFieldMessage = (filed) => {
    let message = `${filed} Should Not Be Empty`;
    return [true, message]
 }

const DeviceRulesSchema = new Schema({
    mac: {type: String, required: requiredFieldMessage('Mac')},
    id: { type: String, default: uuid },
    status: {type: String, default: ''},
    andData: {type: String, default: ''},
    contactNum: {type: String, default: ''},
    forData: {type: String, default: ''},
    ifData: {type: String, default: ''},
    isData: {type: String, default: ''},
    messageOfEmail: {type: String, default: ''},
    messageOfSMS: {type: String, default: ''},
    device_name: {type: String, default: ''},
    ruleName: {type: String, default: ''},
    email: {type: String, default: ''},
    thenData: {type: String, default: ''},
    untilData: {type: String, default: ''},
    valueData: {type: String, default: ''},
    Created: {type: String, default: ''},
    }, options);

module.exports = mongoose.model('tbl_smt400_devicerules', DeviceRulesSchema);
