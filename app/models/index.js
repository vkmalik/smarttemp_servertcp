const db = require('./db');
const DeviceParamHistory = require('./history-table')
const DeviceParam = require('./device-param');
const Schedule = require('./schedule');
const DeviceRules = require('./device-rules')

module.exports = {
	db,
	DeviceParamHistory,
	DeviceParam,
	Schedule,
	DeviceRules
};
