'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = { timestamps: true, versionKey: false };


const requiredFieldMessage = (filed) => {
    let message = `${filed} Should Not Be Empty`;
    return [true, message]
 }


const SMT400Schema = new Schema({
    mac: {type: String, required: requiredFieldMessage('Mac')},   
    MsgID: {type: String, default: ''},
    pair_key: {type: String, default: ''},
    dev_type: {type: String, default: ''},
    dis_dev_name: {type: String, default: ''},
    site_name: {type:String, default: 'Your Site'},
    device_name: {type:String, default: 'Your Device'},
    dev_ver: {type: String, default: ''},
    eheat_mode: {type: String, enum: [0, 1]},
    perimt_mode: {type: String, enum: [0, 1, 2, 3, 4, 5]},
    dry: {type: String, enum: [0,1]},
    equip_mode: {type: String, enum: [0, 1, 2, 3, 4, 5 ,6]},
    equip_status: {type: String, enum: [0, 1, 2, 3]},
    fan_type: {type: String, enum: [0, 1, 2]},
    fan_speed: {type: String, enum: [0, 1, 2, 3, 4, 5, 6, 7]},
    temp_unit: {type: String, default: ''},
    temp_max: {type: String, default: ''},
    temp_min: {type: String, default: ''},
    temp_gap: {type: String, default:''},
    temp_cool: {type: String, default: ''},
    temp_heat: {type: String, default: ''},
    dis_humi: {type: String, default: ''},
    dis_temp: {type: String, default: ''},
    dis_outside: {type: String, default: ''},
    occupied: {type: String, enum: [0, 1]},
    buttonshow: {type: String, enum: [0, 1, 2, 3]},
    fan_active: {type: String, enum: [0, 1]},
    relay_active: {type: String, enum: [0, 1]},
    relay_status: {type: String, enum: [0, 1]},
    wifi_lost: {type: String, enum: [0, 1]},
    aux_relay: {type: String, default: 'Aux relay'},
    isOnline: {type: Boolean, default: false },
    socketIp: {type: String, default: ''},
    socketIpForMob: {type: String, default:''},
    ipDetails: {type: Object, default: {}},
    timeStamp: {type: Number, default: 0},
    updatedFrom: { type: Array, default: [] }
    }, options);







module.exports = mongoose.model('tbl_smt400_devices', SMT400Schema);
