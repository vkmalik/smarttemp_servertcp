'use strict';

const config = {
    db: {
        uri: 'mongodb://user:user123@172.31.5.32:27017/db_smarttemp'
    },

    // JWT Secret
    jwt: {
        secret: (process.env.JWT_SECRET || 'test-jwt-secret'),
        tokenExpirePeriod: (60 * 60 * 1) // 1 day
    },

    // Node MAiler
    nodemailer: {
        user: 'example@gmail.com',
        password: 'example'
    },

    // NODE ENV VARIABLES

    PORT: process.env.PORT || 3000,

    IP: '0.0.0.0',

    // Redirection Host Addresses
    HOST_ADDR: {
        ui: 'http://13.238.58.241:4200',
        server: 'http://13.238.58.241:3000'
    },
};

module.exports = config;
