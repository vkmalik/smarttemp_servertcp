'use strict';

const config = {
    db: {
        uri: process.env.MONGO_DB_URI
    },

    // JWT Secret
    jwt: {
        secret: process.env.JWT_SECRET,
        tokenExpirePeriod: (60 * 60 * 1) // 1 day
    },

    // Node MAiler
    nodemailer: {
        user: 'example@gmail.com',
        password: 'example'
    },

    // NODE ENV VARIABLES

    PORT: process.env.PORT || 3000,

    IP: '0.0.0.0',

    // Redirection Host Addresses
    HOST_ADDR: {
        ui: 'http://ec2-13-59-45-16.us-east-2.compute.amazonaws.com',
        server: 'http://ec2-13-59-45-16.us-east-2.compute.amazonaws.com'
    },

};

module.exports = config;